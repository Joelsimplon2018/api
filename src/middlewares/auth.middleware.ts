import { NextFunction, Request, Response } from "express";
import { isExisted } from "../utils/validators";
import { IRegisterRequestBody, ILoginRequestBody } from "../interfaces/auth.interface";

export const registerMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const body = req.body as IRegisterRequestBody;
  const errors = [] as string[];
  // vérifications si les champs existent
  if (!isExisted(body.email)) errors.push("Champ courriel obligatoire");
  if (!isExisted(body.firstname)) errors.push("Champ prénom obligatoire");
  if (!isExisted(body.lastname)) errors.push("Champ nom obligatoire");
  if (!isExisted(body.pseudo)) errors.push("Champ pseudo obligatoire");
  if (!isExisted(body.password)) errors.push("Champ mot de passe obligatoire");
  if (!isExisted(body.confirm_password)) errors.push("Champ confirmation du mot de passe obligatoire");
  if (!isExisted(body.address)) errors.push("Champ adresse obligatoire");
  if (!isExisted(body.num_address)) errors.push("Champ numéro de l'adresse obligatoire");
  if (!isExisted(body.postal_code)) errors.push("Champ code postal obligatoire");
  if (!isExisted(body.country)) errors.push("Champ pays obligatoire");

  // si y'a des erreurs, on retourne le tableau d'erreurs
  if (errors.length > 0) {
    return res.status(409).json({ error: errors });
  }

  //TODO: vérification des formats

  // vérification si les mots de passes correspondent
  if (body.password !== body.confirm_password) {
    return res.status(409).json({ error: "Les mots de passe ne correspondent pas" });
  }
  next();
};

export const loginMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const body = req.body as ILoginRequestBody;
  const errors = [] as string[];
  // vérifications si les champs existent
  if (!isExisted(body.email)) errors.push("Champ courriel obligatoire");
  if (!isExisted(body.password)) errors.push("Champ mot de passe obligatoire");

  // si y'a des erreurs, on retourne le tableau d'erreurs
  if (errors.length > 0) {
    return res.status(409).json({ error: errors });
  }

  next();
};
