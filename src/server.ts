require("dotenv").config();
import express, { Application } from "express";
import helmet from "helmet";
import http from "http";
import cors from "cors";
import router from "./routes";
import { connectDb } from "./database";
const app: Application = express();
const httpServer = http.createServer(app);
const PORT = process.env.PORT || 5000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(helmet());
app.use(cors());
// routes de l'api
app.use(router);

(async () => {
  try {
    httpServer.listen(PORT);
    connectDb();
    console.log(`Serveur lancé sur le port ${PORT}`);
  } catch (error) {
    console.log("error: ", error);
  }
})();
