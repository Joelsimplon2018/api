import { Request, Response } from "express";
import User from "../models/User";
import { IRegisterRequestBody, ILoginRequestBody } from "../interfaces/auth.interface";
import bcrypt from "bcrypt";
import { createToken } from "../utils/token";

export const registerController = async (req: Request, res: Response) => {
  const body = req.body as IRegisterRequestBody;
  try {
    const emailExist = await User.findOne({ email: body.email });
    // si le courriel existe, on retourne une erreur
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant" });
    }

    // hash du mot de passe
    const hash = await bcrypt.hash(body.password, 10);

    // insertion des données
    const userData = {
      email: body.email,
      firstname: body.firstname,
      lastname: body.lastname.toUpperCase(),
      pseudo: body.pseudo,
      password: hash,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      country: body.country.toUpperCase(),
    };
    const userSaved: any = await new User(userData).save();
    // création du token
    const token = createToken(userSaved);
    if (!token) {
      return res.status(500).json({ error: "Erreur serveur, veuillez vous connecter manuellement" });
    }

    // formattage des données pour la réponse
    const user = {
      firstname: userSaved.firstname,
      lastname: userSaved.lastname,
      pseudo: userSaved.pseudo,
      email: userSaved.email,
    };

    res.status(201).json({ message: "Inscription validée", token, user });
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const loginController = async (req: Request, res: Response) => {
  const body = req.body as ILoginRequestBody;
  try {
    const userExist: any = await User.findOne({ email: body.email });
    // si le courriel existe, on retourne une erreur
    if (!userExist) {
      return res.status(409).json({ error: "Couple courriel/mot de passe incorrect" });
    }

    // hash du mot de passe
    const goodPassword = await bcrypt.compare(body.password, userExist.password);
    if (!goodPassword) {
      return res.status(409).json({ error: "Couple courriel/mot de passe incorrect" });
    }

    // suppression de la référence en faisant un parse stringify pour pouvoir supprimer une clé de l'objet

    // création du token
    const token = createToken(userExist);
    if (!token) {
      return res.status(500).json({ error: "Erreur serveur, veuillez réessayer" });
    }
    // formattage des données pour la réponse
    const user = {
      firstname: userExist.firstname,
      lastname: userExist.lastname,
      pseudo: userExist.pseudo,
      email: userExist.email,
    };

    res.status(200).json({ token, user });
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};
