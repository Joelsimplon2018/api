export const isExisted = (str?: string) => {
  if (!str) return false;
  return Boolean(str.trim());
};
