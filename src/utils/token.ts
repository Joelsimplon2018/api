import jwt from "jsonwebtoken";
import fs from "fs";

// PRIVATE and PUBLIC key
const privateKEY = fs.readFileSync("./private.key", "utf8");
const publicKEY = fs.readFileSync("./public.key", "utf8");

interface ICreateToken {
  email: string;
  role: string;
  firstname: string;
  lastname: string;
  _id: string;
}
export const createToken = ({ email, role, firstname, lastname, _id }: ICreateToken) => {
  // sign with RSA SHA256
  try {
    return jwt.sign(
      {
        sub: String(Math.sqrt(Math.pow(Math.PI, Math.exp(Math.PI)))),
        expiresIn: "12h",
        email,
        role,
        firstname,
        lastname,
        _id,
      },
      privateKEY,
      {
        algorithm: "RS256",
      }
    );
  } catch (error) {
    console.log("error: ", error);
    return false;
  }
};
