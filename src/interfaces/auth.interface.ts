export interface IRegisterRequestBody {
  email: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  password: string;
  confirm_password: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
}

export interface ILoginRequestBody {
  email: string;
  password: string;
}
