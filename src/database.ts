require("dotenv").config();
import mongoose from "mongoose";

export const connectDb = async () => {
  try {
    await mongoose.connect(
      `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.qi0ho.mongodb.net/${process.env.DB_NAME}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      }
    );
    console.log("MongoDB connecté !");
  } catch (error) {
    console.log("erreur connexion MongoDB: ", error);
  }
};
