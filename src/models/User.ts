import mongoose from "mongoose";
// Schéma
const User = new mongoose.Schema(
  {
    email: {
      type: String,
      match: [/\S+@\S+\.\S+/, "courriel invalide"],
      required: [true, "courriel manquant"],
      createIndexes: { unique: true },
    },
    role: {
      type: String,
      default: "CLIENT",
    },
    firstname: {
      type: String,
      required: [true, "Prénom manquant"],
    },
    lastname: {
      type: String,
      required: [true, "Nom manquant"],
    },
    pseudo: {
      type: String,
      required: [true, "Pseudo manquant"],
    },
    password: {
      type: String,
      //match: [/\S+@\S+\.\S+/, "courriel invalide"],
      required: [true, "Mot de passe manquant"],
    },
    address: {
      type: String,
      required: [true, "Adresse manquante"],
    },
    numAddress: {
      type: String,
      required: [true, "Numéro de l'adresse manquante"],
    },
    postalCode: {
      type: String,
      required: [true, "Code postal manquant"],
    },
    country: {
      type: String,
      required: [true, "Pays manquant"],
    },
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("users", User);
