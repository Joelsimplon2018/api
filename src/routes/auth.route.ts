import express from "express";
import { registerController, loginController } from "../controllers/auth.controller";
import { registerMiddleware, loginMiddleware } from "../middlewares/auth.middleware";
const router = express.Router();

/**
 * GET
 */

/**
 * POST
 */
router.post("/login", loginMiddleware, loginController);
router.post("/register", registerMiddleware, registerController);

export default router;
