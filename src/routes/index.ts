import express from "express";
const router = express.Router();
import auth from "./auth.route";

/**
 * PARTIE PUBLIC
 */
router.use("/auth", auth);
export default router;
